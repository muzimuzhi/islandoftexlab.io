+++
title = "TeXplate"
description = "Create document structures."
draft = false
weight = 50
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = '<a href="https://gitlab.com/islandoftex/texplate">texplate</a> is a templating tool specifically aimed at TeX documents.'
toc = true
top = false
readme_url = "https://gitlab.com/islandoftex/texplate/-/blob/master/README.md"
+++
