+++
title = "arara"
description = "TeX automation tool based on rules and directives"
draft = false
weight = 1
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = """\
  <a href="https://gitlab.com/islandoftex/arara">arara</a> is a TeX automation tool based on rules and directives. It gives you a way to enhance your TeX experience.\
  <blockquote>\
	<p>\
	arara has its <a href="https://islandoftex.gitlab.io/arara/">own website</a>.
	Find a <a href="https://islandoftex.gitlab.io/arara/quickstart/">quickstart guide</a>,
	the <a href="https://islandoftex.gitlab.io/arara/manual/">manual</a> and
	<a href="https://islandoftex.gitlab.io/arara/news/">news blog posts</a> there.\
	</p>\
  </blockquote>\
  <br/>\
"""
toc = true
top = false
readme_url = "https://gitlab.com/islandoftex/arara/-/blob/development/README.md"
+++

![arara](https://i.stack.imgur.com/hjUsN.png)
