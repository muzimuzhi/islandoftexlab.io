+++
title = "checkcites"
description = "Detect undefined/unused references from LaTeX auxiliary or bibliography files."
draft = false
weight = 40
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = '<a href="https://gitlab.com/islandoftex/checkcites">checkcites</a> is a Lua script written for the sole purpose of detecting undefined/unused references from LaTeX auxiliary or bibliography files.'
toc = true
top = false
readme_url = "https://gitlab.com/islandoftex/checkcites/-/blob/master/README.md"
+++
