+++
title = "Residents and members"
description = "An island would be less enjoyable without its inhabitants."
draft = false
weight = 20
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Learn about the residents of the island."
toc = true
top = false
+++

# The core team

## Paulo

Our resident duck Paulo is the happy face of the island. Apart from doing great
PR, be it at TUG or in certain popular chat rooms, he is the one who started, or
at least ideated, most of the island's popular tools. `arara` made the start and
many great project ideas and projects followed, and will follow.

## Ben

Ben is the technical lead of the island. With an open eye towards aspiring
technologies and development tooling, he tries to create a modern and organized
working environment. After his full rewrite of `arara`, no complexity can keep
him from making progress – even if it may take some time.

# Project-specific experts

Our small core team is supported by the invaluable help of experts who
participate in depth instead of breadth in the island's projects.

A few noteworthy contributors:

- Yukai Chou (documentation and website)
- Marco Daniel (arara)
- Enrico Gregorio (checkcites)
- Brent Longborough (arara, _in memoriam_)
- Vít Novotný (TeX Live Docker images)
- Marei Peischl (TeX Live Docker images)
- Jonathan Spratte (TeX packages)
- Nicola Talbot (arara)

We thank all our contributors, even those who are not listed here.
