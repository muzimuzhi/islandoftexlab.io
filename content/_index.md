+++
title = "The Island of TeX"

# The homepage contents
[extra]
lead = '<b>The Island of TeX</b> is a warm and welcoming place for TeX-related projects.'
url = "/docs/getting-started/introduction/"
url_button = "About us"

# Menu items
[[extra.list]]
title = "arara"
content = '''
<p><em>Compile your documents in a breeze.</em></p>
<p style="text-align: justify; text-justify: inter-word;">arara is a TeX automation
tool based on rules and directives. It gives you a way to enhance your TeX experience.
The tool is an effort to provide a concise way to automate the daily TeX workflow for
users and also package writers. Users might write their own rules when the provided
ones do not suffice.</p>
<p><a href="https://islandoftex.gitlab.io/community/projects/arara/">Project page</a>
&#10070; <a href="https://gitlab.com/islandoftex/arara">Source code</a></p>
'''

[[extra.list]]
title = "albatross"
content = '''
<p><em>Find your favorite unicode characters in fonts.</em></p>
<p style="text-align: justify; text-justify: inter-word;">Albatross is a command
line tool for finding fonts that contain a given Unicode glyph. It relies on
<a href="https://www.fontconfig.org/">Fontconfig</a>, a library for configuring
and customizing font access. The tool takes a list of glyphs as input (three
formats are supported). It is worth noting that the tool also provides proper
grapheme support.</p>
<p><a href="https://islandoftex.gitlab.io/community/projects/albatross/">Project page</a>
&#10070; <a href="https://gitlab.com/islandoftex/albatross">Source code</a></p>
'''

[[extra.list]]
title = "texdoc-online"
content = '''
<p><em>Power documentation search like <code>texdoc.org</code>.</em></p>
<p style="text-align: justify; text-justify: inter-word;">TeXdoc online is an
online TeX and LaTeX documentation lookup system. It provides a RESTful API
and a self-updating Docker container. The package documentation lookup relies
on the Island's <a href="https://gitlab.com/islandoftex/libraries/texdoc-api">TeXdoc
API</a>. For topic and package listings, as well as recommendations, the system
relies on the CTAN JSON API.</p>
<p><a href="https://islandoftex.gitlab.io/community/projects/texdoc-online/">Project page</a>
&#10070; <a href="https://gitlab.com/islandoftex/images/texdoc-online">Source code</a></p>
'''

[[extra.list]]
title = "checkcites"
content = '''
<p><em>Detect undefined and/or unused references.</em></p>
<p style="text-align: justify; text-justify: inter-word;">checkcites is a
command line tool written for the sole purpose of detecting undefined and/or
unused references from both LaTeX auxiliary or bibliography files. The tool
currently supports both <code>bibtex</code> and <code>biber</code> backends
and uses the generated auxiliary files to start the analysis.</p>
<p><a href="https://islandoftex.gitlab.io/community/projects/checkcites/">Project page</a>
&#10070; <a href="https://gitlab.com/islandoftex/checkcites">Source code</a></p>
'''

[[extra.list]]
title = "TeXplate"
content = '''
<p><em>Create flexible TeX templates.</em></p>
<p style="text-align: justify; text-justify: inter-word;">TeXplate is a tool
for creating document structures based on templates. One can easily extrapolate
the use beyond articles and theses: the tool is powerful enough to generate any
text-based structure, given that a corresponding template exists.</p>
<p><a href="https://islandoftex.gitlab.io/community/projects/texplate/">Project page</a>
&#10070; <a href="https://gitlab.com/islandoftex/texplate">Source code</a></p>
'''

[[extra.list]]
title = "Docker images"
content = '''
<p><em>TeX Live or ConTeXt standalone through Docker.</em></p>
<p style="text-align: justify; text-justify: inter-word;">With the spread of
continuous integration services among TeX users, there is a need to provide
distributions for containerized services. We aim to provide images for the
regular user who wants continuous integration to work like any other TeX
distro.</p>
<p><a href="https://islandoftex.gitlab.io/community/projects/docker/">Project page</a>
&#10070; <a href="https://gitlab.com/islandoftex/images">Source code</a></p>
'''
+++
