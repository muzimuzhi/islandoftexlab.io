#!/usr/bin/env bash
FILENAME="$1"
OUTFILE="$2"

# remove YYYY-MM-dd- = 11 characters from string
_file_name_wo_date="${FILENAME:11}"
# remove .md suffix
_blog_url_mixed_case="${_file_name_wo_date::-3}"
# lower case slug
_blog_url="${_blog_url_mixed_case,,}"

cat "$FILENAME" | tail -n +2 \
                | sed "s|](@/|](https://islandoftex.gitlab.io/arara/|g" \
                | sed 's|toc \?= \?true|template = "blog/page.html"|g' \
                | sed 's|+++|[extra]\nlead="""\n<span class="text-secondary">This is a repost of an <a href="ARARABLOGPOSTURL">arara blog post.</a></span></p><p class="lead">|g' \
                | sed 's/<!-- more -->/"""\n+++/g' \
                | sed "s|ARARABLOGPOSTURL|https://islandoftex.gitlab.io/arara/news/$_blog_url|g" \
                | (echo "+++" && cat) > "$OUTFILE"
