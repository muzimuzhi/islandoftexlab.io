+++
title = "Publications"
+++

* {{ article_badge(year=2022) }} “IoT theatre presents: The Tempest”, TUGboat,
  Volume 23 (2022), No. 2, DOI:
  [10.47397/tb/43-2/tb134island-tempest](https://doi.org/10.47397/tb/43-2/tb134island-tempest),
  [published PDF](https://www.tug.org/TUGboat/tb43-2/tb134island-tempest.pdf),
  abstract:

  > 2021 was a challenging year for the Island of TeX: roadmap changes, lack of
  > resources, server limitations. Yet, resilience, persistence and a bit of
  > good humour made the island even stronger, with new joiners, community
  > support, bold plans and an even brighter future for the TeX ecosystem. And
  > all just in time for celebrating 10 years of arara, our beloved bird!

* {{ talk_badge(year=2022) }}: “IoT theatre presents: The Tempest”, TUG 2022,
  [watch on YouTube](https://youtu.be/kaJrQlCcJCY),
  [corresponding TUGboat article](https://www.tug.org/TUGboat/tb43-2/tb134island-tempest.pdf)

* {{ talk_badge(year=2021) }}: “2020: a year in review, living on an island”,
  TUG 2021, [watch on YouTube](https://youtu.be/ADOO7TTx2-8)

* {{ article_badge(year=2020) }} “The Island of TeX: Developing abroad – your
  next destination”, TUGboat, Volume 41 (2020), No. 2,
  [published PDF](https://www.tug.org/TUGboat/tb41-2/tb128island.pdf),
  abstract:
  
  > The Island of TeX is a collaborative effort to provide a home to
  > community-based TeX projects. This article discusses the Island's long-term
  > goals and how the worldwide community can come aboard and help the
  > organization enhance the TeX experience for everybody, from newbies to power
  > users.
  
* {{ talk_badge(year=2020) }}: “The Island of TeX: Developing abroad – your next
  destination”, TUG 2020,
  [watch on YouTube](https://youtu.be/Pr5WoFQrceI),
  [corresponding TUGboat article](https://www.tug.org/TUGboat/tb41-2/tb128island.pdf)

* {{ article_badge(year=2020) }} “TeXdoc online – a web interface for serving
  TeX documentation”, TUGboat, Volume 41 (2020), No. 3,
  [published PDF](https://www.tug.org/TUGboat/tb41-3/tb129island-texdoc.pdf),
  abstract:
  
  > When looking for TeX-related documentation, users have many options,
  > including running `texdoc` on their local machine, looking up the package at
  > CTAN, or using a service like texdoc.net. As the latter is known for lacking
  > regular updates, the Island of TeX decided to take the opportunity to
  > provide a complete rewrite of the provided service using a RESTful API and a
  > self-updating Docker container.
  
* {{ article_badge(year=2019) }} “Providing Docker images for TeX Live and
  ConTeXt”, TUGboat, Volume 40 (2019), No. 3,
  [published PDF](https://www.tug.org/TUGboat/tb40-3/tb126island-docker.pdf),
  abstract:
  
  > With the spread of version control and continuous integration services among
  > TeX users there is a need to provide TeX distributions for containerized
  > services. As most available images are not updated regularly and many of
  > them lack relevant tools, we aim to provide images for the regular user who
  > wants continuous integration to work like any other TeX distro.

You will find sources to most of the above publications in this website's
[GitLab repository](https://gitlab.com/islandoftex/islandoftex.gitlab.io/-/tree/master/static/publications).
Please note that those sources may not represent the state as published,
especially as they do not include editorial corrections made after submission.
