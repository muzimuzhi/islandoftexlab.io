% arara: pdflatex
% arara: pdflatex
\documentclass[final]{ltugboat}
\def\JVM{\acro{JVM}}
\usepackage{microtype}
\usepackage{graphicx}
\usepackage[breaklinks,hidelinks,pdfa]{hyperref}
\usepackage{cleveref}

\title{Working remotely from an island: \textsf{arara} and other tools}

\author{Island of \TeX\ (developers)}
\EDITORnoaddress
\netaddress{https://gitlab.com/islandoftex}
\personalURL{}

\newcommand{\tool}[1]{{\sffamily#1}}

\begin{document}

\maketitle

\begin{abstract}
Over the last two years, the Island of \TeX\ has complemented the \TeX\ ecosystem with
some auxiliary tools. This article is a short review of the last year's (more or
less) achievements and a preview of upcoming changes.
\end{abstract}

\section{Providing a home for \TeX-related projects}

In 2019, the Island of \TeX\ started as a small group of two friends trying to
improve the \TeX\ ecosystem (cf.\ fig~\ref{fig:logo}). What started as small steps
towards a new \tool{arara} release with some little side-projects became a more and
more interesting place to stop by in 2020.

\begin{figure}[htbp]
\centering
\includegraphics[]{iotlogo}
\caption{The Island of \TeX\ logo.}
\label{fig:logo}
\end{figure}

The last year started off in full preparation of releasing \tool{arara} version
5 in due time for the \TeX\ Live 2020 pretest. A new \tool{arara} version, new
problems. Software engineering becomes “fun” as soon as users are concerned, yet we
are determined to help our users. More about where this view got us and
\tool{arara} in a later section.

Apart from our flagship project, the island focused on new frontiers over the
whole last year. Therefore, we
\begin{itemize}
  \item stabilized \tool{\TeX plate},
  \item created an archive for stale \TeX\ related projects (containing only
  a backup of the \ExTeX\ repository for now) to preserve history,
  \item migrated \tool{checkcites}, a tool to check for missing or unused
  references, to the island,
  \item published the new \tool{albatross} tool, and
  \item worked on publicity.
\end{itemize}

Of our efforts, three projects received a fair share of attention: our Docker
images, the new \tool{\TeX doc online} tool and the shiny little
\tool{albatross}. Let us drop some words about each of these projects before
looking at \tool{arara} and the future.

\section{New tools for a modern \TeX\ ecosystem}

In \TUB~40:3,\footnote{\tbsurl{tug.org/TUGboat/tb40-3/tb126island-docker.pdf}}
we introduced the island's Docker images for easily
reproducible builds as well as a semi-official response to the need
for continuous integration (\acro{CI}). Our
images were among the first using vanilla \TeX~Live, providing the required
tools for running software included in \TeX~Live (Java Virtual Machine (\JVM), Python, etc.).
Additionally, we provide \TeX~Live releases from 2014 on and let the user
decide whether they want to pull all the documentation and source files
into their \acro{CI} configuration.

With attention came the idea to more officially publish our images as
\verb!texlive/texlive!, which we gladly did. Now we are managing the Docker Hub
releases at \url{https://hub.docker.com/r/texlive/texlive}. Although unnoticed
at first, we even saw \acro{DANTE} e.V., the German-speaking \TeX\ user group, basing their
Docker images on ours.

Apart from applications in \acro{CI}, the Docker images have lured us
into creating another tool based on them. \tool{\TeX doc online}, which
we introduced in \TUB's previous
issue,\footnote{\tbsurl{tug.org/TUGboat/tb41-3/tb129island-texdoc.pdf}}
is now the software running \url{https://texdoc.org}, the successor to
\url{https://texdoc.net} (which now redirects),
thanks to Stefan Kottwitz. We have incorporated a few improvements,
among others \acro{HTTPS} support. You can easily host your own
instance.

After finishing \tool{\TeX doc online} to a production-ready degree, the island
turned to the development of another handy tool, \tool{albatross}. This
command line tool, with a silly yet adorable name, solves a very common problem:
finding (system) fonts that provide a certain glyph. Users may provide the glyphs
themselves\Dash e.g, \texttt{\ss}\Dash or their corresponding Unicode code points in
hexadecimal notation\Dash e.g, \texttt{0xDF}. Currently, it is a thin wrapper around
\verb!fc-list! but there are plans to make it even more useful. As it is in
\TeX~Live, you should give it a shot.

\section{\tool{arara}\texorpdfstring{\Dash}{ - }feeling at home on the island}

In 2019, \tool{arara}, the cool \TeX\ automation tool, was one of the first new
citizens of the Island of \TeX. It moved just in time to work on a new
version, version~5. This version was special in many ways, first and foremost as
it followed its predecessor after such a short period of time.\footnote{Some
  members of the \TeX\ community at StackExchange might remember that working on
  \tool{arara} version~4 was one of Paulo's major distractions while writing a
  never-ending thesis.}
Behind the scenes, we finished a major rewrite of \tool{arara}, mainly working on
features from user feedback, especially directory support and the processing
of multiple files.

After releasing version~5 for the \TeX~Live 2020 pretest, we got hooked by the idea
of aligning release schedules of \tool{arara} with \TeX~Live releases. So we had
big plans and started to work on version~6 right after releasing version~5. We
might have been a bit too ambitious, though, as we received some complaints about
a non-working version~5 from our users. Somehow, they caught our
failure to test the new release on some versions of Windows.
Well, everything has been fixed and we were able to move on.

Approximately three quarters of 2020 remained and we tried to make version~6
shine through an enhanced feature set and optimized workflows. Some new features
we want to highlight:\footnote{A detailed discussion of new features is
  in our blog post about \tool{arara}'s new release on its website at
  \url{https://islandoftex.gitlab.io/arara/}.\raggedright}

\begin{itemize}
  \item Preambles (think of that as the commands \tool{arara} will execute on
  that file) have received new options. Among others, you may now define your
  workflows using preambles and set a global default preamble. That way, you may
  now even call \tool{arara} on files without those special \tool{arara} comments
  (directives) and make our tool (auto)magically execute your default preamble.
  Users wanted to be able to switch their editor's default compiler to \tool{arara},
  even for files without explicit statements, and now they can.
  \item You may pass parameters from the outside into your build flow. Call
  \tool{arara} as
\begin{verbatim}
arara -P jobname=thesis file.tex
\end{verbatim}
  and you can receive that parameter within your \tool{arara} rules and directives
  like this (line breaks added for \TUB\ formatting; this directive should be
  on one line):
\begin{verbatim}[\small]
% arara: pdflatex: { options: [
  "-jobname=@{getSession()
                  .get("arg:jobname")}" ] }
\end{verbatim}
  Multiple users have requested being able to parametrize their directives, so we
  finally managed to implement it.
  \item We added eight new rules and improved the rule format. The most frequent
  requests we receive are about supporting new tools in our rule set. We gladly
  add more \TeX\ tools to \tool{arara}, so if you are missing something feel
  free to contact us.
  \item For quite some time, \tool{arara} has been a very powerful tool and has
  been criticized for being so powerful. We now implemented a first draft of a
  safe mode that restricts \tool{arara} in some of the more harmful execution
  steps. Do not expect real safety, though. This is going to take more work to
  prevent obvious malicious behaviour.
  \item One of the most prominent problems with \tool{arara} has been the lack of
  good introductory material, especially as the manual has grown.
  Hence, we now provide a quick start guide for new users. If you do not use
  \tool{arara} yet, maybe this guide is for you. Interested? Simply run
  \texttt{texdoc arara-quickstart} on your local system.
\end{itemize}

The above is only a short excerpt of the change log but probably the
most important changes for users. So to come back to the initial statement: version~6
is a major milestone in many ways but most importantly because the technical
improvements of version~5 allowed us to implement so many features our users
have been waiting for (sometimes for years).

\section{Perspectives}

The Island of \TeX\ will continue to work on improving the \TeX\ ecosystem in
2021. We hope to be as productive as we were in 2020. So let us try to outline
our near-term goals.

First of all, we want to improve \tool{albatross} and \tool{checkcites} to make
them even more useful. For the latter that will most probably include a rewrite.
Anyway, these small helpers are what makes the daily \TeX\ workflow a bit more
fluent so we will try to gather new ideas for little new helpers. We have
welcomed a new member to the island, who will reveal his first IoT tool soon.

However, the major plans and projects are settled around \tool{arara}. In the
long run, we want the tool to provide far more than just a \acro{CLI} tool for compiling
\TeX. We have plans for a documented \API\ for defining \TeX\ flows that is not as
\verb!latexmk!-centred as some of the existing \API{}s floating around and some
kind of \tool{arara} daemon that will be able to translate multiple documents in
parallel.

Furthermore, most of our tools are written in Kotlin and therefore rely on the
\JVM. With the so-called Kotlin multiplatform projects (\acro{MPP}), we will try to get
closer to native performance. Among others, this could also be useful in
scenarios where a computer (or cloud service for
that matter) with a \TeX~Live installation may not run a \JVM.

To wrap this up, we have many plans and hope to realize as much as possible. If
you are interested in helping us develop ideas or even implementing some code:
visas for the island are free and easy to get, so feel free to reach out.

\makesignature
\end{document}
